<?php

namespace JonasSlotte\ValueStore\Tests;

use JonasSlotte\ValueStore\BulkValueRepository;
use Faker\Factory;
use Illuminate\Support\Facades\DB;

class BulkValueRepositoryTest extends TestCase
{
    /**
     * @return BulkValueRepository
     */
    protected function makeRepository()
    {
        return new BulkValueRepository;
    }

    public function testStoreBulkValues()
    {
        $repo = $this->makeRepository();
        $factory = Factory::create();
        $values = [];
        for ($i = 0; $i < 10; $i++) {
            $uuid = uuidString();
            $value = $factory->boolean();
            $repo->putBool($uuid, $value);
            $values[$uuid] = $value;

            $uuid = uuidString();
            $value = $factory->numberBetween();
            $repo->putInt($uuid, $value);
            $values[$uuid] = $value;

            $uuid = uuidString();
            $value = $factory->text(254);
            $repo->putString($uuid, $value);
            $values[$uuid] = $value;
        }
        $repo->save();

        DB::enableQueryLog();
        $results = $repo->all(array_keys($values));
        $queries = collect(DB::getQueryLog());
        DB::disableQueryLog();

        foreach ($values as $key => $value) {
            $this->assertArrayHasKey($key, $results);
            $this->assertEquals($value, $results[$key]);
        }

        $this->assertEquals(4, $queries->count(), "Should execute 1 main query and one per each value type.");
    }
}
