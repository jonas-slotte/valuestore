<?php

namespace JonasSlotte\ValueStore\Tests;

use JonasSlotte\ValueStore\AtomicValueRepository;
use JonasSlotte\ValueStore\Models\BooleanValue;
use JonasSlotte\ValueStore\Models\StringValue;
use JonasSlotte\ValueStore\Models\ValueRoot;

class AtomicValueRepositoryTest extends TestCase
{
    /**
     * @return AtomicValueRepository
     */
    protected function makeRepository()
    {
        return new AtomicValueRepository;
    }

    public function testStoreAtomicBoolean()
    {
        $repo = $this->makeRepository();
        $uuid = uuidString();
        $repo->putBool($uuid, false);

        $this->assertDatabaseHas('boolean_values', [
            'uuid' => $uuid,
            'value' => false
        ]);
        $this->assertDatabaseHas('value_roots', [
            'uuid' => $uuid,
            'type' => 'boolean'
        ]);

        $valueRoot = ValueRoot::findOrFail($uuid);
        $booleanValue = BooleanValue::findOrFail($uuid);
        $this->assertTrue($valueRoot->valueModel->is($booleanValue));

        $this->assertEquals(false, $repo->getBool($uuid));
    }

    public function testStoreAtomicString()
    {
        $repo = $this->makeRepository();
        $uuid = uuidString();
        $repo->putString($uuid, "teststring");

        $this->assertDatabaseHas('string_values', [
            'uuid' => $uuid,
            'value' => "teststring"
        ]);
        $this->assertDatabaseHas('value_roots', [
            'uuid' => $uuid,
            'type' => 'string'
        ]);

        $valueRoot = ValueRoot::findOrFail($uuid);
        $booleanValue = StringValue::findOrFail($uuid);
        $this->assertTrue($valueRoot->valueModel->is($booleanValue));

        $this->assertEquals("teststring", $repo->getString($uuid));
    }
}
