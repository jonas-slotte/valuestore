<?php

namespace JonasSlotte\ValueStore;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\ServiceProvider as Base;
use JonasSlotte\ValueStore\Contracts\AtomicValueStoreInterface;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Filesystem\Filesystem as FilesystemFilesystem;
use Illuminate\Support\Collection;
use JonasSlotte\ValueStore\Contracts\BulkValueStoreInterface;
use JonasSlotte\ValueStore\Models\BooleanValue;
use JonasSlotte\ValueStore\Models\DateTimeValue;
use JonasSlotte\ValueStore\Models\DateValue;
use JonasSlotte\ValueStore\Models\IntegerValue;
use JonasSlotte\ValueStore\Models\StringValue;
use JonasSlotte\ValueStore\Models\TextValue;

class ServiceProvider extends Base
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(AtomicValueStoreInterface::class, function () {
            return new AtomicValueRepository();
        });
        $this->app->alias(AtomicValueStoreInterface::class, 'valuestore.atomic');

        $this->app->singleton(BulkValueStoreInterface::class, function () {
            return new BulkValueRepository();
        });
        $this->app->alias(BulkValueStoreInterface::class, 'valuestore.bulk');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Filesystem $filesystem)
    {
        Relation::morphMap([
            'boolean' => BooleanValue::class,
            'string' => StringValue::class,
            'text' => TextValue::class,
            'integer' => IntegerValue::class,
            'date' => DateValue::class,
            'datetime' => DateTimeValue::class,
            'time' => TimeValue::class,
        ]);

        $this->publishes([
            __DIR__ . '/../database/migrations/create_value_store_tables.php' => $this->getMigrationFileName($filesystem),
        ], 'migrations');
    }

    /**
     * Returns existing migration file if found, else uses the current timestamp.
     *
     * @param Filesystem $filesystem
     * @return string
     */
    protected function getMigrationFileName(Filesystem $filesystem): string
    {
        $timestamp = date('Y_m_d_His');

        return Collection::make($this->app->databasePath() . DIRECTORY_SEPARATOR . 'migrations' . DIRECTORY_SEPARATOR)
            ->flatMap(function ($path) use ($filesystem) {
                return $filesystem->glob($path . '*_create_value_store_tables.php');
            })->push($this->app->databasePath() . "/migrations/{$timestamp}_create_value_store_tables.php")
            ->first();
    }
}
