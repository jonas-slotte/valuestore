<?php

namespace JonasSlotte\ValueStore\Contracts;

/**
 * Interface for single-value r/w
 */
interface AtomicValueStoreInterface
{
    /**
     * Check if key exists
     *
     * @return boolean
     */
    public function exists($id);

    /**
     * Get a string value from the source by id
     *
     * @return string
     */
    public function getString($id);

    /**
     * Set a string value in the source by id
     */
    public function putString($id, string $value);

    /**
     * @return string
     */
    public function getText($id);

    /**
     * Put a string text
     */
    public function putText($id, string $value);

    /**
     * @return int
     */
    public function getInt($id);

    /**
     * Set an integer value
     */
    public function putInt($id, int $value);

    /**
     * @return bool
     */
    public function getBool($id);

    /**
     * @return bool
     */
    public function putBool($id, bool $value);
}
