<?php

namespace JonasSlotte\ValueStore\Contracts;

/**
 * Interface for multiple-value r/w
 */
interface BulkValueStoreInterface
{
    /**
     * Perform all recorded insertions
     *
     * @return void
     */
    public function save();

    /**
     * Retrieve all or nothing
     *
     * @return array
     */
    public function all(array $uuids);
}
