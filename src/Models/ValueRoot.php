<?php

namespace JonasSlotte\ValueStore\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use JonasSlotte\ValueStore\Models\Traits\HasUuidPrimaryKey;

class ValueRoot extends Model
{
    use HasFactory, HasUuidPrimaryKey;

    public $fillable = ["uuid", "type"];
    protected $primaryKey = 'uuid';
    protected $keyType = 'string';
    public $incrementing = false;

    /**
     * Get the owning value model.
     */
    public function valueModel()
    {
        return $this->morphTo(__FUNCTION__, 'type', 'uuid', 'uuid');
    }
}
