<?php

namespace JonasSlotte\ValueStore\Models\Traits;

trait HasUuidPrimaryKey
{
    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->attributes["uuid"];
    }
}
