<?php

namespace JonasSlotte\ValueStore\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StringValue extends ValueModel
{
    protected $casts = [
        'value' => 'string',
    ];
}
