<?php

namespace JonasSlotte\ValueStore\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use JonasSlotte\ValueStore\Models\Traits\HasUuidPrimaryKey;

class ValueModel extends Model
{
    use HasFactory, HasUuidPrimaryKey;

    public $timestamps = false;

    protected $primaryKey = 'uuid';
    protected $keyType = 'string';
    public $incrementing = false;

    public $fillable = ["uuid", "value"];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function ($model) {
            /**
             * Make sure these are explicitly morphmapped
             */
            $typeAlias = array_flip(Relation::morphMap())[get_class($model)];
            ValueRoot::create([
                'uuid' => $model->uuid,
                'type' => $typeAlias,
            ]);
        });
    }
}
