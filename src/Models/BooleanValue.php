<?php

namespace JonasSlotte\ValueStore\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BooleanValue extends ValueModel
{
    protected $casts = [
        'value' => 'boolean',
    ];
}
