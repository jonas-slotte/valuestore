<?php

namespace JonasSlotte\ValueStore\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IntegerValue extends ValueModel
{
    protected $casts = [
        'value' => 'integer',
    ];
}
