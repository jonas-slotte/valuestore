<?php

namespace JonasSlotte\ValueStore\Facades;

use Illuminate\Support\Facades\Facade as Base;

class BulkValueStore extends Base
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return "valuestore.bulk";
    }
}
