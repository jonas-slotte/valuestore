<?php

namespace JonasSlotte\ValueStore\Facades;

use Illuminate\Support\Facades\Facade as Base;

class AtomicValueStore extends Base
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return "valuestore.atomic";
    }
}
