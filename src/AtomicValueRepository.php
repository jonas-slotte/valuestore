<?php

namespace JonasSlotte\ValueStore;

use Illuminate\Support\ServiceProvider;
use JonasSlotte\ValueStore\Models\BooleanValue;
use JonasSlotte\ValueStore\Models\IntegerValue;
use JonasSlotte\ValueStore\Models\StringValue;
use JonasSlotte\ValueStore\Models\TextValue;
use JonasSlotte\ValueStore\Models\ValueRoot;
use JonasSlotte\ValueStore\Contracts\AtomicValueStoreInterface;

class AtomicValueRepository implements AtomicValueStoreInterface
{
    /**
     * Check if key exists
     *
     * @return boolean
     */
    public function exists($id)
    {
        return ValueRoot::where('uuid', $id)->exists();
    }

    /**
     * Get a string value from the source by id
     *
     * @return string
     */
    public function getString($id)
    {
        return StringValue::findOrFail($id)->value;
    }

    /**
     * Set a string value in the source by id
     */
    public function putString($id, string $value)
    {
        StringValue::create([
            'uuid' => $id,
            'value' => $value
        ]);
    }

    /**
     * @return string
     */
    public function getText($id)
    {
        return TextValue::findOrFail($id)->value;
    }

    /**
     * Put a string text
     */
    public function putText($id, string $value)
    {
        TextValue::create([
            'uuid' => $id,
            'value' => $value
        ]);
    }

    /**
     * @return int
     */
    public function getInt($id)
    {
        return IntegerValue::findOrFail($id)->value;
    }

    /**
     * Set an integer value
     */
    public function putInt($id, int $value)
    {
        IntegerValue::create([
            'uuid' => $id,
            'value' => $value
        ]);
    }

    /**
     * @return bool
     */
    public function getBool($id)
    {
        return BooleanValue::findOrFail($id)->value;
    }

    /**
     * @return bool
     */
    public function putBool($id, bool $value)
    {
        BooleanValue::create([
            'uuid' => $id,
            'value' => $value
        ]);
    }
}
