<?php

namespace JonasSlotte\ValueStore;

use JonasSlotte\ValueStore\Models\BooleanValue;
use JonasSlotte\ValueStore\Models\IntegerValue;
use JonasSlotte\ValueStore\Models\StringValue;
use JonasSlotte\ValueStore\Models\TextValue;
use JonasSlotte\ValueStore\Models\ValueRoot;
use JonasSlotte\ValueStore\Contracts\AtomicValueStoreInterface;
use JonasSlotte\ValueStore\Contracts\BulkValueStoreInterface;
use JonasSlotte\ValueStore\Models\ValueModel;

class BulkValueRepository implements BulkValueStoreInterface, AtomicValueStoreInterface
{
    /**
     * An array of insertions to perform
     *
     * @var array
     */
    protected $insertions = [];

    /**
     * Record an insertion by storing the unsaved model
     *
     * @param ValueModel $model
     */
    protected function recordInsertion(ValueModel $model)
    {
        $this->insertions[$model->uuid] = $model;
    }

    /**
     * Perform all recorded insertions
     *
     * @return void
     */
    public function save()
    {
        foreach ($this->insertions as $key => $insert) {
            $insert->save();
        }
    }

    /**
     * Retrieve all or nothing
     *
     * @return array
     */
    public function all(array $uuids)
    {
        $roots = ValueRoot::with('valueModel')->whereIn('uuid', $uuids)->get();

        return $roots->mapWithKeys(function (ValueRoot $m) {
            return [$m->getUuid() => $m->valueModel->value];
        })->toArray();
    }
    /**
     * Check if key exists
     *
     * @return boolean
     */
    public function exists($id)
    {
        return ValueRoot::where('uuid', $id)->exists();
    }

    /**
     * Get a string value from the source by id
     *
     * @return string
     */
    public function getString($id)
    {
        return StringValue::findOrFail($id)->value;
    }

    /**
     * Set a string value in the source by id
     */
    public function putString($id, string $value)
    {
        $this->recordInsertion(StringValue::make([
            'uuid' => $id,
            'value' => $value
        ]));
    }

    /**
     * @return string
     */
    public function getText($id)
    {
        return TextValue::findOrFail($id)->value;
    }

    /**
     * Put a string text
     */
    public function putText($id, string $value)
    {
        $this->recordInsertion(TextValue::make([
            'uuid' => $id,
            'value' => $value
        ]));
    }

    /**
     * @return int
     */
    public function getInt($id)
    {
        return IntegerValue::findOrFail($id)->value;
    }

    /**
     * Set an integer value
     */
    public function putInt($id, int $value)
    {
        $this->recordInsertion(IntegerValue::make([
            'uuid' => $id,
            'value' => $value
        ]));
    }

    /**
     * @return bool
     */
    public function getBool($id)
    {
        return BooleanValue::findOrFail($id)->value;
    }

    /**
     * @return bool
     */
    public function putBool($id, bool $value)
    {
        $this->recordInsertion(BooleanValue::make([
            'uuid' => $id,
            'value' => $value
        ]));
    }
}
